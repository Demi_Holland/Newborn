<?php

namespace App\Http\Controllers;

use App\User;
use Auth;
use Illuminate\Http\Request;

class UserEditController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function edit($id)
    {
        $user = User::find($id);

        if(Auth::user()->role == 'kantoor'){
            return view('kantoor.users.edit', compact('user'));
        } else {
            return redirect('/');
        }
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|exists:users',
            'password' => 'sometimes',
        ]);

        // Update User
        $user = User::find($id);
        $user->name = $request->input('name');
        $user->email = $request->input('email');

        if(!empty($request->input('password')))
        {
            $user->password = bcrypt($request->input('password'));
        }
        $user->save();


        return redirect('user/edit/'.$id)->with('status', 'User Updated');
    }
}
