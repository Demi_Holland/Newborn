<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Appointment;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        switch (Auth::user()->role) {
            case 'admin':
                return view('auth.register');
                break;
            case 'verloskundigen':
                $appointment_request = Appointment::all();
                return view('verloskundigen.home', compact('appointment_request'));
                break;
            case 'kantoor':
                return view('kantoor.customers.create');
                break;
            default:
                Auth::logout();
                return redirect('/login')->with('error', 'Er is iets mis gegaan! Probeer opnieuw.');
        }
    }
}
