<?php

namespace App\Http\Middleware;

use Closure;

class IsVerloskundigen
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->check() && $request->user()->role == 'verloskundigen') {
            return redirect()->guest('home');
        }

        return $next($request);
    }
}
