<?php

use App\Customer;
use Illuminate\Support\Facades\Input;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

// Home page
Route::get('/', 'HomeController@index');

// Customers
Route::get('customers/create', 'CustomerController@create')->name('customercreate');
Route::post('customers/create', 'CustomerController@store');

Route::get('customers/index', 'CustomerController@index')->name('customerindex');
Route::delete('customers/delete/{id}', 'CustomerController@destroy')->name('customerdestroy');

Route::get('customers/edit/{id}', 'CustomerController@edit')->name('customeredit');
Route::post('customers/edit/{id}', 'CustomerController@update');

Route::get('customers/overview', 'CustomerController@overview')->name('customeroverview');
Route::any('customers/index',function(){
    $q = Input::get ( 'q' );
    $customer_request = Customer::where('name','LIKE','%'.$q.'%')->get();

    return view('kantoor.customers.index', compact('customer_request'));
});

// Appointments
Route::get('appointment/create', 'AppointmentController@create')->name('appointmentcreate');
Route::post('appointment/create', 'AppointmentController@store');

Route::get('appointment/edit/{id}', 'AppointmentController@edit')->name('appointmentedit');
Route::post('appointment/edit/{id}', 'AppointmentController@update');

// Edit user
Route::get('user/edit/{id}', 'UserEditController@edit')->name('usersedit');
Route::post('user/edit/{id}', 'UserEditController@update');